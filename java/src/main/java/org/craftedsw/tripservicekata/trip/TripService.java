package org.craftedsw.tripservicekata.trip;

import java.util.ArrayList;
import java.util.List;

import org.craftedsw.tripservicekata.exception.UserNotLoggedInException;
import org.craftedsw.tripservicekata.user.User;
import org.craftedsw.tripservicekata.user.UserSession;

public class TripService {
    public static final ArrayList<Trip> EMPTY_TRIP_LIST = new ArrayList<Trip>();
    private final ILoggedUserLoader loggedUserLoader;
    private final IFindTrips findTrips;

    public TripService() {
        this.loggedUserLoader = UserSession.getInstance();
        this.findTrips = new TripDAO();
    }

    public TripService(ILoggedUserLoader loggedUserLoader, IFindTrips findTrips) {
        this.loggedUserLoader = loggedUserLoader;
        this.findTrips = findTrips;
    }

    public List<Trip> getTripsByUser(User user)
            throws UserNotLoggedInException {
        User loggedUser = loggedUserLoader.getLoggedUser();

        if (loggedUser == null) {
            throw new UserNotLoggedInException();
        }

        if (user.isFriendWith(loggedUser)) {
            return findTripsByUser(user);
        }
        return EMPTY_TRIP_LIST;
    }

    public List<Trip> findTripsByUser(User user) {
        return findTrips.tripsByUser(user);
    }
}
