package org.craftedsw.tripservicekata.trip;

import org.craftedsw.tripservicekata.user.User;

public interface ILoggedUserLoader {
    User getLoggedUser();
}
