package org.craftedsw.tripservicekata.trip;

import org.craftedsw.tripservicekata.exception.UserNotLoggedInException;
import org.craftedsw.tripservicekata.user.User;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class TripServiceTest {
    private static final User NO_LOGGED_USER = null;
    private static final User LOGGED_USER_WITH_NO_FRIENDS = new User();

    @Test(expected = UserNotLoggedInException.class)
    public void shouldThrowUserNotLoggedInException() {
        ILoggedUserLoader loggedUserLoader = Mockito.mock(ILoggedUserLoader.class);
        Mockito.when(loggedUserLoader.getLoggedUser()).thenReturn(NO_LOGGED_USER);

        IFindTrips findTrips = Mockito.mock(IFindTrips.class);
        Mockito.when(findTrips.tripsByUser(Mockito.any(User.class))).thenReturn(Collections.<Trip>emptyList());

        TripService tripService = new TripService(loggedUserLoader, findTrips);
        tripService.getTripsByUser(new User());
    }

    @Test()
    public void shouldReturnEmptyTripListWhenUserHasNoFriends() {
        ILoggedUserLoader loggedUserLoader = Mockito.mock(ILoggedUserLoader.class);
        Mockito.when(loggedUserLoader.getLoggedUser()).thenReturn(LOGGED_USER_WITH_NO_FRIENDS);

        IFindTrips findTrips = Mockito.mock(IFindTrips.class);
        Mockito.when(findTrips.tripsByUser(Mockito.any(User.class))).thenReturn(Collections.<Trip>emptyList());

        User user = new User();
        user.addTrip(new Trip());
        TripService tripService = new TripService(loggedUserLoader, findTrips);

        List<Trip> trips = tripService.getTripsByUser(user);

        assertThat(trips).isEmpty();
    }

    @Test()
    public void shouldReturnEmptyTripListWhenUserIsFriendWithLoggedUser() {
        User loggedUser = new User();

        ILoggedUserLoader loggedUserLoader = Mockito.mock(ILoggedUserLoader.class);
        Mockito.when(loggedUserLoader.getLoggedUser()).thenReturn(loggedUser);

        IFindTrips findTrips = Mockito.mock(IFindTrips.class);
        Mockito.when(findTrips.tripsByUser(Mockito.any(User.class))).thenReturn(Collections.<Trip>emptyList());

        User friend = new User();
        friend.addFriend(loggedUser);

        TripService tripService = new TripService(loggedUserLoader, findTrips);
        List<Trip> trips = tripService.getTripsByUser(friend);

        assertThat(trips).isEmpty();
    }

    @Test()
    public void shouldReturnNotEmptyTripListWhenUserIsFriendWithLoggedUser() {
        User loggedUser = new User();
        ILoggedUserLoader loggedUserLoader = Mockito.mock(ILoggedUserLoader.class);
        Mockito.when(loggedUserLoader.getLoggedUser()).thenReturn(loggedUser);

        User friend = new User();
        friend.addFriend(new User());
        friend.addFriend(loggedUser);
        friend.addTrip(new Trip());

        IFindTrips findTrips = Mockito.mock(IFindTrips.class);
        Mockito.when(findTrips.tripsByUser(Mockito.any(User.class))).thenReturn(friend.trips());

        TripService tripService = new TripService(loggedUserLoader, findTrips);
        List<Trip> trips = tripService.getTripsByUser(friend);

        assertThat(trips).isNotEmpty();
    }
}
